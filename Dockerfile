# stage 1
FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build --prod

# stage 2docker
FROM nginx:alpine
COPY --from=node /app/dist/AngularDemo /usr/share/nginx/html